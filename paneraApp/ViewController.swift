//
//  ViewController.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import UIKit

class ViewController: UIViewController {

    let tableView = UITableView()
    let parse = CSVParser()
    let analytics = AdobeAnalyticsHelper()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = Palette.c_FEFAE0
        tableView.register(MyCell.self, forCellReuseIdentifier: "ruid")

        tableView.topAnchor.constraint(equalTo:view.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo:view.bottomAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo:view.leftAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo:view.rightAnchor).isActive = true

        tableView.delegate = self
        tableView.dataSource = self

        tableView.separatorColor = Palette.c_BC6C25

        analytics.setState(pageName: "Document List", contextData: ["cd.appPageView": "1"])
    }


}

extension ViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        parse.items[indexPath.row].detailsShown = !parse.items[indexPath.row].detailsShown
        let item = parse.items[indexPath.row]
        analytics.trackAction(actionName: "\(item.year), \(item.month)", contextData: [:])
        tableView.reloadRows(at: [indexPath], with: .left)
    }
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        parse.items.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ruid") as? MyCell else {
            return UITableViewCell()
        }
        let item = parse.items[indexPath.row]
        cell.label.text = "\(item.year), \(item.month)"
        cell.badge.text = "\(item.pfbti)%"
        cell.badge.isHidden = !item.detailsShown
        return cell
    }




}
