//
//  MyCell.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import UIKit

class MyCell: UITableViewCell {

    let stackView = UIStackView()

    let label = UILabel()
    let badge = UILabel()

    required init?(coder: NSCoder) {
        fatalError("Something wrong happened")
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.backgroundColor = Palette.c_DDA15E

        stackView.axis = .horizontal
        stackView.spacing = 20
        stackView.alignment = .fill
        stackView.distribution = .fillEqually

        label.textColor = Palette.c_283618
        label.textAlignment = .center

        badge.textColor = Palette.c_283618
        badge.backgroundColor = Palette.c_606C38
        badge.textAlignment = .center
        badge.layer.cornerRadius = 15
        badge.layer.masksToBounds = true

        contentView.addSubview(stackView)

        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(badge)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        stackView.topAnchor.constraint(equalTo:contentView.topAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo:contentView.bottomAnchor).isActive = true
        stackView.leftAnchor.constraint(equalTo:contentView.leftAnchor).isActive = true
        stackView.rightAnchor.constraint(equalTo:contentView.rightAnchor).isActive = true
        
    }


}
