//
//  AdobeAnalyticsHelper.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import Foundation

class AdobeAnalyticsHelper {
    public func setState(pageName: String, contextData: [String: String]) {
        print("Page name: \(pageName), context: \(contextData)")
    }
    public func trackAction(actionName: String, contextData: [String: String]) {
        print("Action name: \(actionName), context: \(contextData)")
    }
}
