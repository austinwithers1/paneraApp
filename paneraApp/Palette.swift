//
//  Palette.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import UIKit

class Palette {

    //FEFAE0= tableview background
    static let c_FEFAE0 = UIColor(named: "FEFAE0")!
    //283618= cell title font color
    static let c_283618 = UIColor(named: "283618")!
    //DDA15E= cell background color
    static let c_DDA15E = UIColor(named: "DDA15E")!

    static let c_606C38 = UIColor(named: "606C38")!
    static let c_BC6C25 = UIColor(named: "BC6C25")!

}
