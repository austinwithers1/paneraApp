//
//  CSVParser.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import Foundation

struct DataBlob {
    let year: String
    let month: String
    let tisria: String
    let issria: String
    let pfbti: String

    var detailsShown = false
}

class CSVParser {

    var items = [DataBlob]()
    let analytics = AdobeAnalyticsHelper()

    init() {
        let fileURL = Bundle.main.url(forResource: "fy12-onward-rib-filed-via-internet", withExtension: "csv")

        let data = try? Data(contentsOf: fileURL!)
        guard let data else {
            analytics.trackAction(actionName: "CSV Loading Error", contextData: [
                "cd.csvLoadingError":"1",
                "cd.csvLoadingErrorDetails": "LocalErrorDescription"]
            )
            
            return
        }
        let dataString = String(data: data, encoding: .utf8)!

        let lines = dataString.components(separatedBy: "\n")
        var firstLine = true

        for line in lines {

            if firstLine {
                firstLine = false
                continue
            }

            let partz = line.matchingStrings(regex: "\\s*(\"[^\"]*\"|[^,]+)\\s*")

            guard partz.count > 1 else { continue }

            let newBlob = DataBlob(
                year: partz[0],
                month: partz[1],
                tisria: partz[2],
                issria: partz[3],
                pfbti: partz[4]
            )

            items.append(newBlob)
        }
    }
}
