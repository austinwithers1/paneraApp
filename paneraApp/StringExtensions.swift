//
//  StringExtensions.swift
//  paneraApp
//
//  Created by AustinWithers on 10/20/22.
//

import Foundation

//Borrowed from https://stackoverflow.com/questions/27880650/swift-extract-regex-matches

extension String {

    func matchingStrings(regex: String) -> [String] {
        guard let regex = try? NSRegularExpression(pattern: regex, options: []) else { return [] }
        let nsString = self as NSString
        let results  = regex.matches(in: self, options: [], range: NSMakeRange(0, nsString.length))
        return results.map { result in
            (0..<result.numberOfRanges).map {
                result.range(at: $0).location != NSNotFound
                    ? nsString.substring(with: result.range(at: $0))
                    : ""
            }
        }.reduce([]) { partialResult,next in
            var nextResult = partialResult
            nextResult.append(next[0])
            return nextResult
        }
    }
}
